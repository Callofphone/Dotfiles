#!/bin/sh
picom -b --config /home/jamiel/.config/picom/picom.conf &
ibus-daemon -drx &
nm-applet &
blueman-applet &
mictray &
redshift &
