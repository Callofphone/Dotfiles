from typing import List  # noqa: F401
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Screen, KeyChord, ScratchPad, DropDown
from libqtile.lazy import lazy
import subprocess,re

mod = "mod4"
terminal = "alacritty" 

keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down(),
        desc="Move focus down in stack pane"),
    Key([mod], "j", lazy.layout.up(),
        desc="Move focus up in stack pane"),

    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down(),
        desc="Move window down in current stack "),
Key([mod, "control"], "j", lazy.layout.shuffle_up(),
        desc="Move window up in current stack "),
    Key([mod, "control"], "h", lazy.layout.shrink(),
             lazy.layout.increase_nmaster(),
        desc="Resizes window to the left"),
Key([mod, "control"], "l", lazy.layout.grow(),
             lazy.layout.decrease_nmaster(),
        desc="Move window up in current stack "),
    # Spawn programs
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "b", lazy.spawn("brave"), desc="Launch browser"),
    Key([mod], "p", lazy.spawn("/home/jamiel/.config/dmenu/nerdtree-bookmarks.sh"), desc="Launch bookmark-script"),
    Key([mod], "r", lazy.spawn("thunar"), desc="Launch file manager"),
    Key([mod], "n", lazy.spawn("notion-app"), desc="Launch Notion"),
    #Spawn scratchpads
    Key([mod, "shift"], "Return", lazy.group['scratchpad'].dropdown_toggle('term'), desc="Launch terminal scratchpad"),
    Key([mod, "shift"], "k", lazy.group['scratchpad'].dropdown_toggle('keepass'), desc="Launch Keepass scratchpad"),
    Key([mod, "shift"], "b", lazy.group['scratchpad'].dropdown_toggle('browser'), desc="Launch browser scratchpad"),
    Key([mod, "shift"], "f", lazy.group['scratchpad'].dropdown_toggle('file-browser'), desc="Launch file browser"),
    Key([mod, "shift"], "m", lazy.group['scratchpad'].dropdown_toggle('music'), desc="Launch music player"),

    # Special keys
    Key(
        [], "XF86AudioRaiseVolume",
        lazy.spawn("amixer set Master 5%+ unmute")
    ),
    Key(
        [], "XF86AudioLowerVolume",
        lazy.spawn("amixer set Master 5%- unmute")
    ),
    Key(
        [], "XF86AudioMute",
        lazy.spawn("amixer -c 0 -q set Master toggle")
    ),
    Key([], 'XF86MonBrightnessUp',   lazy.spawn("xbacklight -inc 5")),
    Key([], 'XF86MonBrightnessDown', lazy.spawn("xbacklight -dec 5")),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "shift"], "r", lazy.restart(), desc="Restart qtile"),
    Key([mod, "shift"], "l", lazy.shutdown(), desc="Shutdown qtile"),
    Key([mod, "shift"], "p", lazy.spawn("dmenu_run -h 24"),
        desc="Spawn a command using a prompt widget"),

    KeyChord([mod], "o", [Key([], "k", lazy.spawn("keepassxc &"))])
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

# Scratchpads
groups.extend([
        ScratchPad("scratchpad", [
                DropDown("term", terminal, opacity=0.9, height=0.8, y=0.1),
                DropDown("browser", "firefox-developer-edition", opacity=1, height=0.8, y=0.1),
                DropDown("music", "spotify", opacity=1, height=0.8, y=0.1),
                DropDown("file-browser", "thunar", opacity=1, height=0.8, y=0.1),
                DropDown("keepass", "keepassxc", opacity=1, height=0.8, y=0.1),
            ])
    ])

layouts = [
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Bsp(),
    # layout.Columns(),
    # layout.Matrix(),
    layout.MonadTall(margin = 8),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

import os

@hook.subscribe.startup_once
def start_once():
    autostart_file = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([autostart_file])

widget_defaults = dict(
    font='sans',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentLayout(),
                widget.GroupBox(),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Volume(),
                widget.Systray(),
                widget.Backlight(
                    backlight_name='intel_backlight',
                ),
                widget.Battery(),
                widget.Clock(format='%Y-%m-%d %a %I:%M %p'),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"
wmname = "LG3D"
